/**
 * @file
 * @brief C11 Thread Library
 */
#ifndef _THREADS_H_
#define _THREADS_H_

#ifdef __linux__
    #include <pthread.h>
#endif

#ifdef __cplusplus
extern "C"{
#endif

#if   defined(__linux__)
    #define THRDS_CALL
#elif defined(_WIN32)
    #define THRDS_CALL __cdecl
#else
    #define THRDS_CALL
#endif

#if   defined(__linux__)

    // Thread function.
    typedef int(THRDS_CALL *thrd_start_t)(void*);

    /// Thread identifier.
    typedef pthread_t thrd_t;

#elif defined(_WIN32)

    // Thread function.
    typedef int(THRDS_CALL *thrd_start_t)(void*);

    /// Thread identifier.
    typedef void* thrd_t;

#else
    #error No implementation on this platform!
#endif

/**
 * @brief Result codes.
 */
enum
{
    thrd_success,   ///< Indicates successful return value.
    thrd_timedout,  ///< Indicates timed out return value.
    thrd_busy,      ///< Indicates unsuccessful return value due to resource temporary unavailable.
    thrd_nomem,     ///< Indicates unsuccessful return value due to out of memory condition.
    thrd_error,     ///< Indicates unsuccessful return value.
};

int THRDS_CALL thrd_create( thrd_t *thr, thrd_start_t func, void *arg );
int THRDS_CALL thrd_equal( thrd_t lhs, thrd_t rhs );
thrd_t THRDS_CALL thrd_current( void );
void THRDS_CALL thrd_exit( int res );
int THRDS_CALL thrd_detach( thrd_t thr );
int THRDS_CALL thrd_join( thrd_t thr, int *res );

/**
 * @class mtx_t
 * @brief Mutex.
 */
typedef union mtx_t
{
    long dummy;     // This value is used to force memory aligned with long integer.
    char data[40];
} mtx_t;

/**
 * @brief Mutex types.
 */
enum
{
    mtx_plain       = 0x00, ///< Normal mutex.
    mtx_recursive   = 0x01, ///< Recursive mutex.
    mtx_timed       = 0x02, ///< Timeout supportd mutex.
};

int THRDS_CALL mtx_init( mtx_t* mutex, int type );
void THRDS_CALL mtx_destroy( mtx_t* mutex );

int THRDS_CALL mtx_lock( mtx_t* mutex );
int THRDS_CALL mtx_trylock( mtx_t* mutex );
int THRDS_CALL mtx_unlock( mtx_t* mutex );

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
